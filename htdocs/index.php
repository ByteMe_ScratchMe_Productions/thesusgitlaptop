<?php 
error_reporting(0);
// db name/ xamp sql
$db = new mysqli('localhost','root','','nathan_thesis',3306); 
//bring all players
if (isset($_GET) && ($_GET['operation']=="select"))
{
    //get the values for unity
    $sqlstatement = "select id,headX,headY,headZ,rAnkleX,rAnkleY,rAnkleZ,midHipX,midHipY,midHipZ,rToeX,rToeY,rToeZ,rKneeX,rKneeY,rKneeZ,rHipX,rHipY,rHipZ,rElbowX,rElbowY,rElbowZ,rWristX,rWristY,rWristZ,rThumbX,rThumbY,rThumbZ,rPinkyX,rPinkyY,rPinkyZ,rIndexX,rIndexY,rIndexZ,exerciseId from coord";
 
    //this will get me the list of players in the database
    $players = $db->query($sqlstatement);
 
    while($row = $players->fetch_assoc())
    {
        echo "%".$row["id"]."|".
        $row["headX"]."|".
		$row["headY"]."|".
		$row["headZ"]."|".
		$row["rAnkleX"]."|".
		$row["rAnkleY"]."|".
		$row["rAnkleZ"]."|".
		$row["midHipX"]."|".
		$row["midHipY"]."|".
		$row["midHipZ"]."|".
		$row["rToeX"]."|".
		$row["rToeY"]."|".
		$row["rToeZ"]."|".
		$row["rKneeX"]."|".
		$row["rKneeY"]."|".
		$row["rKneeZ"]."|".
		$row["rHipX"]."|".
		$row["rHipY"]."|".
		$row["rHipZ"]."|".
		$row["rElbowX"]."|".
		$row["rElbowY"]."|".
		$row["rElbowZ"]."|".
		$row["rWristX"]."|".
		$row["rWristY"]."|".
		$row["rWristZ"]."|".
		$row["rThumbX"]."|".
		$row["rThumbY"]."|".
		$row["rThumbZ"]."|".
		$row["rPinkyX"]."|".
		$row["rPinkyY"]."|".
		$row["rPinkyZ"]."|".
		$row["rIndexX"]."|".
		$row["rIndexY"]."|".
		$row["rIndexZ"]."|".
		$row["exerciseId"]."|";
    }
}

//new player joins game
if (isset($_GET) && ($_GET['operation']=="insert")){
	
	$headX = mysqli_real_escape_string($db,$_GET['headX']);
	$headY = mysqli_real_escape_string($db,$_GET['headY']);
	$headZ = mysqli_real_escape_string($db,$_GET['headZ']);
	$rAnkleX = mysqli_real_escape_string($db,$_GET['rAnkleX']);
	$rAnkleY = mysqli_real_escape_string($db,$_GET['rAnkleY']);
	$rAnkleZ = mysqli_real_escape_string($db,$_GET['rAnkleZ']);
	$midHipX = mysqli_real_escape_string($db,$_GET['midHipX']);
	$midHipY = mysqli_real_escape_string($db,$_GET['midHipY']);
	$midHipZ = mysqli_real_escape_string($db,$_GET['midHipZ']);
	$rToeX = mysqli_real_escape_string($db,$_GET['rToeX']);
	$rToeY = mysqli_real_escape_string($db,$_GET['rToeY']);
	$rToeZ = mysqli_real_escape_string($db,$_GET['rToeZ']);
	$rKneeX = mysqli_real_escape_string($db,$_GET['rKneeX']);
	$rKneeY = mysqli_real_escape_string($db,$_GET['rKneeY']);
	$rKneeZ = mysqli_real_escape_string($db,$_GET['rKneeZ']);
	$rHipX = mysqli_real_escape_string($db,$_GET['rHipX']);
	$rHipY = mysqli_real_escape_string($db,$_GET['rHipY']);
	$rHipZ = mysqli_real_escape_string($db,$_GET['rHipZ']);
	$rElbowX = mysqli_real_escape_string($db,$_GET['rElbowX']);
	$rElbowY = mysqli_real_escape_string($db,$_GET['rElbowY']);
	$rElbowZ = mysqli_real_escape_string($db,$_GET['rElbowZ']);
	$rWristX = mysqli_real_escape_string($db,$_GET['rWristX']);
	$rWristY = mysqli_real_escape_string($db,$_GET['rWristY']);
	$rWristZ = mysqli_real_escape_string($db,$_GET['rWristZ']);
	$rThumbX = mysqli_real_escape_string($db,$_GET['rThumbX']);
	$rThumbY = mysqli_real_escape_string($db,$_GET['rThumbY']);
	$rThumbZ = mysqli_real_escape_string($db,$_GET['rThumbZ']);
	$rPinkyX = mysqli_real_escape_string($db,$_GET['rPinkyX']);
	$rPinkyY = mysqli_real_escape_string($db,$_GET['rPinkyY']);
	$rPinkyZ = mysqli_real_escape_string($db,$_GET['rPinkyZ']);
	$rIndexX = mysqli_real_escape_string($db,$_GET['rIndexX']);
	$rIndexY = mysqli_real_escape_string($db,$_GET['rIndexY']);
	$rIndexZ = mysqli_real_escape_string($db,$_GET['rIndexZ']);
	$exerciseId = mysqli_real_escape_string($db,$_GET['exerciseId']);
	
	$sqlstatement = "insert into coord(headX,headY,headZ,rAnkleX,rAnkleY,rAnkleZ,midHipX,midHipY,midHipZ,rToeX,rToeY,rToeZ,rKneeX,rKneeY,rKneeZ,rHipX,rHipY,rHipZ,rElbowX,rElbowY,rElbowZ,rWristX,rWristY,rWristZ,rThumbX,rThumbY,rThumbZ,rPinkyX,rPinkyY,rPinkyZ,rIndexX,rIndexY,rIndexZ,exerciseId)
		values ('$headX','$headY','$headZ','$rAnkleX','$rAnkleY','$rAnkleZ','$midHipX','$midHipY','$midHipZ','$rToeX','$rToeY','$rToeZ','$rKneeX','$rKneeY','$rKneeZ','$rHipX','$rHipY','$rHipZ','$rElbowX','$rElbowY','$rElbowZ','$rWristX','$rWristY','$rWristZ','$rThumbX','$rThumbY','$rThumbZ','$rPinkyX','$rPinkyY','$rPinkyZ','$rIndexX','$rIndexY','$rIndexZ','$exerciseId')";
	if ($db->query($sqlstatement))
		{
			//return the ID of the newly created user to Unity
			echo "OK|".$db->insert_id."|";
		}
		else {
			echo $sqlstatement;
			echo $db->error();
		}	
}
?>
<html>
	<head>
		<title>Co-Ordinates</title>
	</head>
	<body>
		<p>
		Hi my name is Nathan This is just to save co-ordinates Pls do not mind me.
		</p>
	</body>
</html>